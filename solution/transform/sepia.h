#ifndef SEPIA_H
#define SEPIA_H

#include <stdbool.h>

#include "../image/image.h"

void sepia_filter(struct image * target);
void sepia_filter_asm(struct image * target);

#endif // SEPIA_H