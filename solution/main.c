#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>
#include "bmp/bmp.h"
#include "cli/cli.h"
#include "image/pixel.h"
#include "transform/sepia.h"
#include "utils/utils.h"

static const char* read_errors[] = {
    [READ_OK]="success",
    [READ_FAILED_TO_ALLOCATE_MEMORY]="not enough memory",
    [READ_INVALID_BITS]="invalid image"
};

static const char* write_errors[] = {
    [WRITE_OK]="success",
    [WRITE_ERROR]="failed to write"
};

static struct timeval get_current_time() {
    struct rusage r;
    getrusage(RUSAGE_SELF, &r);
    return r.ru_utime;
}

static long get_perfomance(struct timeval start) {
    const struct timeval end = get_current_time();

    return ((end.tv_sec - start.tv_sec) * 1000000L) +
        end.tv_usec - start.tv_usec;
}

int main(int argc, char* argv[]) {
    if (argc != 3 && argc != 4) return -1;

    int use_asm = 0;
    const char* input_filename = argv[1];
    const char* output_filename = argv[2];
    
    if (argc == 4 && strcmp(argv[1], "-asm") == 0) { 
	    use_asm = 1;
        input_filename = argv[2];
        output_filename = argv[3];		
    }
    
    FILE* input_bmp = fopen(input_filename, "rb");

    if (input_bmp == NULL) error("Unable to open file: %s\n", input_filename);

    FILE* output_bmp = fopen(output_filename, "wb");

    if (output_bmp ==  NULL) {
        fclose(input_bmp);
        error("Unable to open file: %s\n", output_filename);
    }

    struct image target;

    enum read_status read_code = from_bmp(input_bmp, &target);

    if (read_code != READ_OK) {
        destroy_image(&target);
        fclose(input_bmp);
        fclose(output_bmp);
        error("Read BMP failed:\n%s\n", read_errors[read_code]);
    }

    const struct timeval start = get_current_time();
    
    if (use_asm == 0) 
      sepia_filter(&target);
    else
      sepia_filter_asm(&target);      

    const long perfomance = get_perfomance(start);

    enum write_status write_code = to_bmp(output_bmp, &target);

    if (write_code != WRITE_OK) {
        destroy_image(&target);
        fclose(input_bmp);
        fclose(output_bmp);
        error("Write BMP failed:\n%s\n", write_errors[write_code]);
    }

    destroy_image(&target);
    fclose(input_bmp);
    fclose(output_bmp);

    printf("Time to do this job:\n %ld\n", perfomance);

    return 0;
}


