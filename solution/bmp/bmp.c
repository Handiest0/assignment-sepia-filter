#include "bmp.h"

#include "../image/pixel.h"

#define BMP_SIGNATURE 0x4D42
#define BMP_BIT_COUNT 24
#define BMP_SIZE 40
#define BMP_PLANES 1
#define BMP_RESERVED_ZERO 0
#define ALIGN 3

enum read_status from_bmp(FILE* in, struct image* image) {
    struct bmp_header header;

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_BITS;
	if (header.bfType != BMP_SIGNATURE) return READ_INVALID_BITS;
	if (header.biBitCount != BMP_BIT_COUNT || header.biPlanes != BMP_PLANES) return READ_INVALID_BITS;
	if (!create_image(image, header.biWidth, header.biHeight)) return READ_INVALID_BITS;

    for (uint64_t i = 0; i != header.biHeight; i++)
    {
        if (fread(get_pixel(image, 0, i), sizeof(struct pixel), header.biWidth, in) != header.biWidth){return READ_INVALID_BITS;}
		if (header.biWidth % 4 != 0 && fseek(in, (4 - (int32_t)((header.biWidth * sizeof(struct pixel)) % 4)), SEEK_CUR) != 0){return READ_INVALID_BITS;}
    }

    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* image)
{
    char empty[ALIGN] = { 0 };
    struct bmp_header header;
	uint64_t adjust_width = image->width + (image->width % 4 == 0 ? 0 : 4 - (image->width * sizeof(struct pixel)) % 4);

    header.bfType = BMP_SIGNATURE;
    header.bFileSize = sizeof(struct bmp_header) + image->height * adjust_width * sizeof(struct pixel);
    header.bfReserved = BMP_RESERVED_ZERO;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = BMP_SIZE;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = BMP_PLANES;
    header.biBitCount = BMP_BIT_COUNT;
    header.biCompression = BMP_RESERVED_ZERO;
    header.biSizeImage = image->height * adjust_width * sizeof(struct pixel);
    header.biXPelsPerMeter = BMP_RESERVED_ZERO;
    header.biYPelsPerMeter = BMP_RESERVED_ZERO;
    header.biClrUsed = BMP_RESERVED_ZERO;
    header.biClrImportant = BMP_RESERVED_ZERO;

    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;

    for (uint64_t i = 0; i != image->height; i++)
    {
        if (fwrite(get_pixel(image, 0, i), sizeof(struct pixel), image->width, out) != image->width) return WRITE_ERROR;
		if (image->width % 4 != 0 && fwrite(empty, sizeof(char), 4 - (image->width * sizeof(struct pixel) % 4), out) != 4 - (image->width * sizeof(struct pixel) % 4))
            return WRITE_ERROR;
    }

    return WRITE_OK;
}

