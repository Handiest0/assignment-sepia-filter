#ifndef UTILS_H
#define UTILS_H

_Noreturn void error(const char* msg, ...);

#endif // UTILS_H