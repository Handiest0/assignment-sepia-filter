NASM=name -f elf64
CC=gcc
CFLAGS= -O3

all:
    $(NASM) ./solution/transform/sepia.asm
    $(CC) -c ./solution/*.c ./solution/*/*.c
    $(CC) ./solution/*.o ./solution/*/*.o -o sepia
clean:
    rm *.o